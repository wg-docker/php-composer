FROM php:8.0-fpm-alpine

RUN apk add --no-cache \
					git

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync \
	&& install-php-extensions \
                    bcmath \
                    exif \
                    gd \
                    imagick \
                    intl \
                    pdo_mysql \
                    redis \
                    zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
